---
title: "ACS National Meeting & Exposition - Fall 2019"
subtitle: "San Diego, CA"
output: github_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## MIT MLPDS
* [**M**achine **L**earning for **P**harmaceutical **D**iscovery and **S**ynthesis Consortium](https://mlpds.mit.edu/) is a collaboration between the pharmaceutical and biotechnology industries and the departments of Chemical Engineering, Chemistry, and Computer Science at the Massachusetts Institute of Technology. This collaboration will facilitate the design of useful software for the automation of small molecule discovery and synthesis.  
* [**chemprop**](https://github.com/swansonk14/chemprop) (message passing neural networks for molecular property prediction) makes use of data from [MoleculeNet](http://moleculenet.ai/), a benchmark for molecular machine learning.

## MOLVEC (NCATS)
* [**M**olecular **O**ptical **L**ine **V**ectorization of **E**xtracted **C**hemicals](https://spotlite.nih.gov/ncats/molvec) = a chemical OCR engine: vectorize chemical images into chemical objects, preserving 2D layout.  
* [MOLVEC demo](https://molvec.ncats.io/)  
* [**N**ational **C**enter for **A**dvancing **T**ranslational **S**ciences](https://spotlite.nih.gov/ncats)  
* [Poster](https://tripod.nih.gov/interns/JustinSzajek)  

## SciWalker (ontochem)
* [SciWalker](https://www.sciwalker.com/sciwalker/faces/search.xhtml): text mining, data extraction and knowledge prediction for the natural and material sciences & beyond.  
* [ontochem](https://www.ontochem.de/): company website  
* [OCMiner](https://www.ontochem.de/our-products/information-discovery.html): use case = automatic identification of relevant chemical compounds from patents

## POET
* [**P**otential **O**ptimization by **E**volutionary **T**echniques](https://gitlab.com/muellergroup/poet) is an open-source software package for discovering simple, fast, and accurate many-body interatomic potential models using genetic programming.  
* [Mueller Group](http://muellergroup.jhu.edu/Research.html)  

## DINGOS.py
* [**D**esign of **I**nnovative **N**CEs **G**enerated by **O**ptimization **S**trategies](https://codeocean.com/capsule/8271554/tree/v1)
* Molecular design by hybrid machine intelligence and rule-driven chemical synthesis
* **D**esign **o**f **G**enuine **S**tructures (DOGS): reaction-driven *de novo* design of bioactive compounds